print("Enter certain year to check how much leap-year passed until selected and counting")


def leap_year_counter(selected_year_not_checked=input()):
    # constants
    first_divider = fd = 4
    second_divider = sd = 100
    third_divider = td = 400
    if selected_year_not_checked.isdigit():  # TODO Search about isnumeric and change isdigit to isnumeric
        selected_year = int(selected_year_not_checked)
    else:
        print("Entered data is not numeral or negative!")
        return
    if selected_year == 0:
        print("There is no 0th year")
        return
    print(str(selected_year // fd - selected_year // sd + selected_year // td) + " leap-years passed until this year and counting")


leap_year_counter()
