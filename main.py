print("Enter certain year to check is it leap-year or not")


def leap_year_detection(selected_year_not_checked=input()):
    # constants
    first_divider = fd = 4
    second_divider = sd = 100
    third_divider = td = 400
    if selected_year_not_checked.isdigit():  # TODO Search about isnumeric and change isdigit to isnumeric
        selected_year = int(selected_year_not_checked)
    else:
        print("Entered data is not numeral or negative!")
        return
    if selected_year == 0:
        print("There is no 0th year")
        return
    if selected_year % td == 0 or selected_year % fd == 0 and not selected_year % sd == 0:
        state_of_year = "leap-year"
    else:
        state_of_year = "usual year"
    print(str(selected_year) + " year is " + state_of_year)


leap_year_detection()
